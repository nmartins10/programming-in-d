import std.stdio;

void main() {
  write("How many students are there? ");
  
  int studentCount;
  readf("%s", &studentCount);
  
  writeln("Got it: There are ", studentCount, " students.");
}