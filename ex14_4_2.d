import std.stdio;

void main() {
	int secret;
	
	while (secret <= 0 || secret > 10) {
		write("Anna, enter a number from 1 to 10: ");
		readf(" %s", &secret);
	}
	
	int guess;
	while (guess != secret) {
		write("Bill, enter your guess: ");
		readf(" %s", &guess);
	}
	
	writeln("CORRECT: the number is ", secret);
}
