import std.stdio;

void main() {
	int size;
	
	write("Enter the number of elements you want to put: ");
	readf(" %s", &size);
	
	double[] elements;
	elements.length = size;
	
	int i;
	while (i < size) {
		write("Enter the number ", i + 1, ": ");
		readf(" %s", &elements[i]);
		++i;
	}
	
	writeln("Sorting elements: ");
	elements.sort;
	
	i = 0;
	while (i < size) {
		write(elements[i], " ");
		++i;
	}
	
	writeln();
	
	writeln("Reversing elements: ");
	elements.reverse;
	
	i = 0;
	while (i < size) {
		write(elements[i], " ");
		++i;
	}
	
	writeln();
}
