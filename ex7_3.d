import std.stdio;

void main() {
  writeln("---bool properties:---");
  writeln("Type           : ", bool.stringof);
  writeln("Length in bytes: ", bool.sizeof);
  writeln("Minimum value  : ", bool.min);
  writeln("Maximum value  : ", bool.max);
  writeln();
  
  writeln("---byte properties:---");
  writeln("Type           : ", byte.stringof);
  writeln("Length in bytes: ", byte.sizeof);
  writeln("Minimum value  : ", byte.min);
  writeln("Maximum value  : ", byte.max);
  writeln();
  
  writeln("---ubyte properties:---");
  writeln("Type           : ", ubyte.stringof);
  writeln("Length in bytes: ", ubyte.sizeof);
  writeln("Minimum value  : ", ubyte.min);
  writeln("Maximum value  : ", ubyte.max);
  writeln();
  
  writeln("---short properties:---");
  writeln("Type           : ", short.stringof);
  writeln("Length in bytes: ", short.sizeof);
  writeln("Minimum value  : ", short.min);
  writeln("Maximum value  : ", short.max);
  writeln();
  
  writeln("---ushort properties:---");
  writeln("Type           : ", ushort.stringof);
  writeln("Length in bytes: ", ushort.sizeof);
  writeln("Minimum value  : ", ushort.min);
  writeln("Maximum value  : ", ushort.max);
  writeln();
  
  writeln("---int properties:---");
  writeln("Type           : ", int.stringof);
  writeln("Length in bytes: ", int.sizeof);
  writeln("Minimum value  : ", int.min);
  writeln("Maximum value  : ", int.max);
  writeln();
  
  writeln("---uint properties:---");
  writeln("Type           : ", uint.stringof);
  writeln("Length in bytes: ", uint.sizeof);
  writeln("Minimum value  : ", uint.min);
  writeln("Maximum value  : ", uint.max);
  writeln();
  
  writeln("---long properties:---");
  writeln("Type           : ", long.stringof);
  writeln("Length in bytes: ", long.sizeof);
  writeln("Minimum value  : ", long.min);
  writeln("Maximum value  : ", long.max);
  writeln();
  
  writeln("---ulong properties:---");
  writeln("Type           : ", ulong.stringof);
  writeln("Length in bytes: ", ulong.sizeof);
  writeln("Minimum value  : ", ulong.min);
  writeln("Maximum value  : ", ulong.max);
  writeln();
  
  writeln("---float properties:---");
  writeln("Type           : ", float.stringof);
  writeln("Length in bytes: ", float.sizeof);
  writeln("Minimum value  : ", float.min_normal);
  writeln("Maximum value  : ", float.max);
  writeln();
  
  writeln("---double properties:---");
  writeln("Type           : ", double.stringof);
  writeln("Length in bytes: ", double.sizeof);
  writeln("Minimum value  : ", double.min_normal);
  writeln("Maximum value  : ", double.max);
  writeln();
  
  writeln("---real properties:---");
  writeln("Type           : ", real.stringof);
  writeln("Length in bytes: ", real.sizeof);
  writeln("Minimum value  : ", real.min_normal);
  writeln("Maximum value  : ", real.max);
  writeln();
  
  writeln("---ifloat properties:---");
  writeln("Type           : ", ifloat.stringof);
  writeln("Length in bytes: ", ifloat.sizeof);
  writeln("Minimum value  : ", ifloat.min_normal);
  writeln("Maximum value  : ", ifloat.max);
  writeln();
  
  writeln("---idouble properties:---");
  writeln("Type           : ", idouble.stringof);
  writeln("Length in bytes: ", idouble.sizeof);
  writeln("Minimum value  : ", idouble.min_normal);
  writeln("Maximum value  : ", idouble.max);
  writeln();
  
  writeln("---ireal properties:---");
  writeln("Type           : ", ireal.stringof);
  writeln("Length in bytes: ", ireal.sizeof);
  writeln("Minimum value  : ", ireal.min_normal);
  writeln("Maximum value  : ", ireal.max);
  writeln();
  
  writeln("---cfloat properties:---");
  writeln("Type           : ", cfloat.stringof);
  writeln("Length in bytes: ", cfloat.sizeof);
  writeln("Minimum value  : ", cfloat.min_normal);
  writeln("Maximum value  : ", cfloat.max);
  writeln();
  
  writeln("---cdouble properties:---");
  writeln("Type           : ", cdouble.stringof);
  writeln("Length in bytes: ", cdouble.sizeof);
  writeln("Minimum value  : ", cdouble.min_normal);
  writeln("Maximum value  : ", cdouble.max);
  writeln();
  
  writeln("---creal properties:---");
  writeln("Type           : ", creal.stringof);
  writeln("Length in bytes: ", creal.sizeof);
  writeln("Minimum value  : ", creal.min_normal);
  writeln("Maximum value  : ", creal.max);
  writeln();
  
  writeln("---char properties:---");
  writeln("Type           : ", char.stringof);
  writeln("Length in bytes: ", char.sizeof);
  writeln("Minimum value  : ", char.min);
  writeln("Maximum value  : ", char.max);
  writeln();
  
  writeln("---wchar properties:---");
  writeln("Type           : ", wchar.stringof);
  writeln("Length in bytes: ", wchar.sizeof);
  writeln("Minimum value  : ", wchar.min);
  writeln("Maximum value  : ", wchar.max);
  writeln();
  
  writeln("---dchar properties:---");
  writeln("Type           : ", dchar.stringof);
  writeln("Length in bytes: ", dchar.sizeof);
  writeln("Minimum value  : ", dchar.min);
  writeln("Maximum value  : ", dchar.max);
  writeln();
  
  writeln("---void properties:---");
  writeln("Type           : ", void.stringof);
  writeln("Length in bytes: ", void.sizeof);
  writeln();
}