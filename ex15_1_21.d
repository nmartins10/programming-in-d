import std.stdio;

void main() {
	int width = 40000;
	int length = 60000;
	int areaPerTree = 1000;
	
	//int treesNeeded = width * length / areaPerTree; this overflows
	int treesNeeded = width / areaPerTree * length;
	
	writeln("Number of trees needed: ", treesNeeded);
}
