import std.stdio;

void main() {
	write("Enter the dice value: ");
	int diceValue;
	readf(" %s", &diceValue);
	
	if(diceValue >=1 && diceValue <= 3) {
		writeln("You won");
	} else if (diceValue >= 4 && diceValue <= 6) {
		writeln("I won");
	} else {
		writeln("ERROR: ", diceValue,  " is invalid");
	}
}
